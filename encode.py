#!/usr/bin/env python
# -*- coding: utf-8 -*-

#For fileimport
import csv
import os.path

#sets for containing data
keywordSet = set()
allwordSet = set()

#file that holds all possible 6 letter combinations in the english language
keyfilePath = 'C:/Users/foa/OneDrive/learn/python/keywordSet.txt'
#a text file with all the english words (or close to it)
allWordsFilePath = 'C:/Users/foa/OneDrive/learn/python/allwords.txt'

#opens and adds words to the sets

with open(keyfilePath,'r', newline='\n') as csvFile:
    for row in csv.reader(csvFile, delimiter=','):
        for word in row:
            keywordSet.add(word.lower())

with open(allWordsFilePath,'r', newline='\n') as csvFile:
    for row in csv.reader(csvFile, delimiter=','):
        for word in row:
            allwordSet.add(word)

#your code
"""Vigenere encoding, by Arve Seljebu(arve@seljebu.no), MIT License, 2014"""

alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅabcdefghijklmnopqrstuvwxyzæøå .,?-_;:+1234567890"'

def vigenere_encode(msg, key):
    """Function that encodes a string with Vigenere cipher. The encrypted
       string is returned.
    """
    secret = ''
    key_length = len(key)
    alphabet_length = len(alphabet)

    for i, char in enumerate(msg):
        msgInt = alphabet.find(char)
        encInt = alphabet.find(key[i % key_length])

        if msgInt == -1 or encInt == -1:
            return ''

        encoded = (msgInt + encInt) % alphabet_length
        secret += alphabet[encoded]

    return secret

#end your code

#modified to decode
def vigenere_decode(encmsg, key):
    plain = ''
    key_length = len(key)
    alphabet_length = len(alphabet)

    for i, char in enumerate(encmsg):
        encMsgInt = alphabet.find(char)
        encInt = alphabet.find(key[i % key_length])

        if encMsgInt == -1 or encInt == -1:
            return ''

        decoded = (encMsgInt - encInt) % alphabet_length
        plain += alphabet[decoded]

    return plain

#test encode and decrypt
message = 'My first computer program was a song called Popcorn written in QBasic. The second computer program I made was a bot made for IRC.'
keyword = 'source'

encrypted = vigenere_encode(message, keyword)
print(encrypted)

decrypted = vigenere_decode(encrypted, keyword)
print(decrypted)

#decode string
decodeString = 'q0Ø:;AI"E47FRBQNBG4WNB8B4LQN8ERKC88U8GEN?T6LaNBG4GØ""N6K086HB"Ø8CRHW"+LS79Ø""N29QCLN5WNEBS8GENBG4FØ47a'

#holds the highest number of word matches
highest = 0

#holds the guessed messages
guessedMessage = ''
guessedKey = ''

#brute forces by decoding using all keys and keeping the one with the most matches
for possiblekey in keywordSet:
    decodedmessage = vigenere_decode(decodeString, possiblekey)
    decodedWordList = decodedmessage.lower().split()
    decodedWordSet = set(decodedWordList)
    
    matchedSet = decodedWordSet.intersection(allwordSet)

    if len(matchedSet) > highest:
        highest = len(matchedSet)
        guessedMessage = decodedmessage
        guessedKey = possiblekey

#prints the guessed matches
print(guessedMessage)
print(guessedKey)

decodeString = 't-JO:BK0aM,:CQ+ÆAGW?FJGB0KVCGMQ6SQN"GAIDL-PÅ7954E:7Jr,IÆoCF0M"CQdØVlHD53CÅ;IA2DMG5ØHDØVåL:JQØ439LRBBVEMTBÆ6CF0M"CQNAG8G1V6LÅ8FF4Z'

#holds the highest number of word matches
highest = 0

#holds the guessed messages
guessedMessage = ''
guessedKey = ''

#brute forces by decoding using all keys and keeping the one with the most matches
for possiblekey in allwordSet:
    decodedmessage = vigenere_decode(decodeString, possiblekey)
    decodedWordList = decodedmessage.lower().split()
    decodedWordSet = set(decodedWordList)
    
    matchedSet = decodedWordSet.intersection(allwordSet)

    if len(matchedSet) > highest:
        highest = len(matchedSet)
        guessedMessage = decodedmessage
        guessedKey = possiblekey

#prints the guessed matches
print(guessedMessage)
print(guessedKey)